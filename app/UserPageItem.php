<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPageItem extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function history()
    {
        return $this->hasMany('App\UserItemHistory');
    }

    public function userPage()
    {
        return $this->belongsTo('App\UserPage');
    }

    public function globalItem()
    {
        return $this->belongsTo('App\GlobalItem');
    }
}
