<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserItemHistory extends Model
{
    public function userpageItem()
    {
        return $this->belongsTo('App\UserPageItem');
    }
}
