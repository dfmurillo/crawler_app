<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Facades\App\Services\Crawl;

use App\UserAccess;
use App\UserGroup;
use App\GlobalPage;

use Debugbar;

class AdminController extends Controller
{
    public function access()
    {
        $accessList = UserAccess::all();
        return view('pages.admin.access', compact('accessList'));
    }

    public function pages()
    {
        $groups = UserGroup::with(['userPages.globalPage','userPages.userItems.globalItem'])->get();
        $global = GlobalPage::with(['items'])->get();
        return view('pages.admin.pages', compact('groups', 'global'));
    }
}
