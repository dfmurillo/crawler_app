<?php

namespace App\Http\Controllers\Admin\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\UserAccess;
use Auth;
use Validator;

class UserAccessController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'domain'=>'required|url',
            'status'=>'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return ['success'=>false, 'error'=>$validator->errors()];
        } else {
            $access = new UserAccess;
            $access->domain = $request->input('domain');
            $access->status = $request->input('status');
            $access->user_id = Auth::id();
            $access->token = bin2hex(random_bytes(16));
            $access->save();
            $success = true;

            return compact('success', 'access');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $access = UserAccess::find($id);

        if($request->has('status')) {
            $access->status = $request->input('status');
        }
        if($request->has('token')) {
            $access->token = bin2hex(random_bytes(16));
        }
        
        $access->save();

        return compact('access');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
