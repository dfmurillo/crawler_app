<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPage extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function globalPage()
    {
        return $this->belongsTo('App\GlobalPage');
    }

    public function userGroup()
    {
        return $this->belongsTo('App\UserGroup');
    }

    public function userItems()
    {
        return $this->hasMany('App\UserPageItem');
    }
}
