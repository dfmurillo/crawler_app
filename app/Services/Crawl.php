<?php

namespace App\Services;

use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Client;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

use App\GlobalPage;
use App\GlobalItem;

class Crawl
{
	private $groupPage = '';

	public function getFinalPrice(GlobalPage $groupPage)
	{
		$this->groupPage = $groupPage;
		$pageHtml = $this->curlCall();
		if($groupPage->type_request === 'service'){
			$values = $this->jsonRequest($pageHtml);
		}
		else{
			$itemsValue = $this->crawlRequest($pageHtml);
			$values = $this->cleanValue($itemsValue);
		}

		$this->SaveValue($values);

		return $values;
	}

	public function curlCall()
	{
		$url = $this->groupPage->protocol.'://'.$this->groupPage->host.$this->groupPage->page;
		$client = new Client;
		$onRedirect = function( RequestInterface $request, ResponseInterface $response, UriInterface $uri) {
			echo 'Redirecting! ' . $request->getUri() . ' to ' . $uri . "<br>";
		};

		$options = [
			'allow_redirects' => [
				'max' => 10, 
				'track_redirects' => true,
				'on_redirect' => $onRedirect,
			],
			'debug'=>true,
			'headers'=>[
				// 'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Crihrome/67.0.3396.62 Safa/537.36',
				'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
				'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
				'Accept-Encoding' => 'gzip, deflate, br',
			],
			'synchronous'=>true
		];

		if($this->groupPage->cookies){
			$jar = new CookieJar;
			$userCookie = json_decode($this->groupPage->cookies, true);
			$options['cookies'] = $jar->fromArray($userCookie['cookies'], $userCookie['path']);
		}

		try {
			$request = $client->request('GET', $url, $options);
			
			// The returned page body.
			$body = $request->getBody();
			$html = $body->getContents();
			
			
			if($this->groupPage->type_request === 'service'){
				return json_decode($html, true);
			} else {
				dd([$url, $html]);
				return $html;
			}

		} catch (RequestException $e) {
			$log = new PageCrawlLog;
			$log->setConnection($this->conn);
			$log->group_page_id = $this->groupPage->id;
			$log->success = false;
			$log->detail = ''.$e->getMessage();
			$log->activity = 'crawling';
			$log->save();
			// echo '<h1>Error</h1>';
			// echo '<pre>';
			// print_r($e->getRequest());
			// echo '</pre>';
			// echo '<pre> Msg: ';
			// print_r($e->getMessage());
			// echo '</pre>';
			// if ($e->hasResponse()) {
			// 	//dd($e->getResponse());
			// 	dd('hola');
			// } else {
			// 	echo "<p>No response</p>";
			// 	dd($e->getRequest());
			// }
		}

	}

	public function jsonRequest($json)
	{
		$response = [];
		foreach ($this->useData as $key => $item) {
			$response[$key]['obj'] = $item;
			$response[$key]['final'] = $json[$item['dom_item']];
		}

		return $response;
	}

	public function crawlRequest($html)
	{
		$crawler = new Crawler($html);
		$response = [];

		foreach ($this->useData as $key => $item) {
			$response[$key]['obj'] = $item;

			$el = $crawler->filter($item['dom_item']);			
			if($item['dom_item_eq'])
				$el = $el->eq($item['dom_item_eq']);
			
			try {

				if($item['dom_attr'])
						$response[$key]['value'] = $el->attr($item['dom_attr']);
				else 
					$response[$key]['value'] = $el->html();	

			} catch(\InvalidArgumentException $e){
				$log = new PageCrawlLog;
				$log->setConnection($this->conn);
				$log->group_page_id = $this->groupPage->id;
				$log->success = false;
				$log->detail = ''.$e->getMessage();
				$log->activity = 'dom error';
				$log->save();
			}
			
		}

		return $response;
	}

	private function cleanValue($itemData)
	{

		foreach ($itemData as $key => $item) {
			if(isset($item['value'])){
				switch ($item['obj']['data_clean']) {
					case 'numeric':
						$cleanValue = trim(preg_replace('/[^0-9\.,]/', '', $item['value']));
	
						//clean thousands
						$cleanValue = trim(strip_tags(str_replace($item['obj']['thousand_separator'], '', $cleanValue)));
	
						//default decimal will be , so if is different, change . to ,
						$globalCommaSep = config('app.price_separator');
						if($item['obj']['comma_separator'] != $globalCommaSep){
							$cleanValue = trim(str_replace($item['obj']['comma_separator'], $globalCommaSep, $cleanValue));            
						}
	
						$itemData[$key]['final'] = floatval($cleanValue);
						break;
					case 'alpha':
						$cleanValue = trim(preg_replace('/\s+/', ' ', $item['value'])); //clean n and t
						$itemData[$key]['final'] = $cleanValue;
						break;
					default: //basic
						$cleanValue = trim(preg_replace('/\s+/', ' ', $item['value'])); //clean n and t
						$itemData[$key]['final'] = $cleanValue;
						break;
				}
			}

		}

		return $itemData;
	}

	private function SaveValue($values)
	{
		\Debugbar::addMessage('Start Save');
		\Debugbar::info($values);
		
		foreach ($values as $key => $item) {
			$saveLog = false;
			$userPageHistory = new ItemHistory;
			$userPageHistory->setConnection($this->conn);
			$userPageHistory->page_item_id = $item['obj']['id'];
			if(isset($item['final'])){
				$userPageHistory->value = $item['final'];
			} else {
				$saveLog = true;
			}
			$userPageHistory->save();

			if($saveLog === true){
				$log = new PageCrawlLog;
				$log->setConnection($this->conn);
				$log->group_page_id = $this->groupPage->id;
				$log->success = false;
				$log->detail = 'No final value';
				$log->activity = 'no value';
				$log->save();
			}

			\Debugbar::addMessage('Save Hist');
			\Debugbar::info($userPageHistory);
			
		}
	}

	private function SetUseData(GroupPage $groupPage){
		$this->groupPage = $groupPage;
		
		$this->useData = [];

		foreach ($groupPage->pageItems as $key => $item) {

			$domData = $item;
			if($item->common_item_id)
				$domData = $item->commonItem;
			
			$this->useData[$key]['id'] = $item->id;
			$this->useData[$key]['item_name'] = $domData->item_name;
			$this->useData[$key]['dom_item'] = $domData->dom_item;
			$this->useData[$key]['dom_item_eq'] = $domData->dom_item_eq;
			$this->useData[$key]['dom_attr'] = $domData->dom_attr;
			$this->useData[$key]['comma_separator'] = $domData->comma_separator;
			$this->useData[$key]['thousand_separator'] = $domData->thousand_separator;
			$this->useData[$key]['data_clean'] = $domData->data_clean;
		}
	}
}
