<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function userPages()
    {
        return $this->hasMany('App\UserPage');
    }
}
