<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalItem extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function page()
    {
        return $this->belongsTo('App\GlobalPage');
    }

    public function userItems()
    {
        return $this->hasMany('App\UserPageItem');
    }
}
