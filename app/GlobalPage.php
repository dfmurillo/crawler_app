<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalPage extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function items()
    {
        return $this->hasMany('App\GlobalItem');
    }
    
    public function userPages()
    {
        return $this->hasMany('App\UserPage');
    }
}
