import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        accesses: [],
        groups: [],
        global: [],
    },
    getters: {
        accesses: (state) => {
            return state.accesses
        },
        groups: (state) => {
            return state.groups
        },
        global: (state) => {
            return state.global
        },
    },
    mutations: {
        addAccess: (state, access) => {
            state.accesses.push(access);
        },
        deleteAccess (state, id) {
            state.accesses = state.accesses.filter(obj => obj.id !== id);
        },
        addGroup: (state, {group, pos = 'last'}) => {
            if(pos === 'last')
                state.groups.push(group);
            else 
                state.groups.unshift(group);
        },
        addGlobal: (state, global) => {
            state.global = global;
        },
    },
    actions: {
        setAccess: ({commit, state}, newAccess) => {
            commit('addAccess', newAccess);
            return state.accesses;
        },
        setGroup: ({commit, state}, {newGroup, pos = 'last'}) => {
            commit('addGroup', {group: newGroup, pos: pos});
            return state.groups;
        },
        setGlobal: ({commit, state}, newGlobal) => {
            commit('addGlobal', newGlobal);
            return state.global;
        },
    },
});