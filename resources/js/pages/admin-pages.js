require('../bootstrap');
window.Vue = require('vue');
import store from '../store';

Vue.component('group-list', require('../components/GroupList.vue'));
Vue.component('group-add', require('../components/GroupAdd.vue'));

const app = new Vue({
    el: '#app',
    store,
});
