require('../bootstrap');
window.Vue = require('vue');
import store from '../store';

Vue.component('access-add', require('../components/AccessAdd.vue'));
Vue.component('access-list', require('../components/AccessList.vue'));

const app = new Vue({
    el: '#app',
    store
});
