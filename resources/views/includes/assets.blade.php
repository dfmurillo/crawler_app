@section('assets_header')
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Styles -->
  <link rel="stylesheet" href="https://bootswatch.com/4/journal/bootstrap.min.css">
  <link rel="stylesheet" href="{{ mix('/css/main.css') }}">

  {{-- <link rel="stylesheet" href="{{ mix('/css/vendors.css') }}"> --}}
  @yield('page_css')

  <script src="https://cdn.polyfill.io/v2/polyfill.min.js" defer></script>
  @endsection
  
  @section('assets_footer')
  
  <script src="{{ mix('/js/manifest.js') }}" defer></script>
  <script src="{{ mix('/js/vendor.js') }}" defer></script>
            
  <script type="text/javascript">
    window._URL = (function(){var _url = {};var SetUrl = function(key, value){_url[key] = value;};var GetUrl = function(key, params){var params = params || [];var retUrl = _url[key];for (var key in params) {if (params.hasOwnProperty(key)) {var element = params[key];var reg = new RegExp('!'+key+'!');retUrl = retUrl.replace(reg, element);}}return retUrl;};return {set: SetUrl,get: GetUrl}})();
    _URL.set('app', '{{ url('/') }}');
  </script>

  @yield('page_js')
@endsection