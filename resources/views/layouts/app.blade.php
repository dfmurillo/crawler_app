@include('includes.assets')
@include('includes.tracking')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Crawler') }} - @yield('title')</title>

    @yield('assets_header')
    @yield('page_header')
</head>
<body>
    <div id="app">
        
        @include('modules.header')
        
        @yield('content')
        
        @include('modules.footer')
        
    </div>
    @yield('assets_footer')
    @yield('page_footer')
</body>
</html>
