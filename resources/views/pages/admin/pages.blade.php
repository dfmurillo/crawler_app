@extends('layouts.app')
@section('title', 'Crawler Admin')

@section('content')
    
    <div class="container">
        <br><br>
        <div class="row">
            <div class="col-md-3">
                <group-add></group-add>
            </div>
            <div class="col-md-9">
                <group-list :list="{{ $groups }}" :global="{{ $global }}"></group-list>
            </div>
        </div>
    </div>

@endsection


@section('page_footer')
    <script>
        _URL.set('group.store', '{{ route('group.store') }}');
        _URL.set('group.update', '{{ route('group.update', ['!group!' => '!group!']) }}');
    </script>
    <script src="{{ mix('/js/admin-pages.js') }}" defer></script>
@endsection

