@extends('layouts.app')
@section('title', 'Crawler Admin')


@section('content')
    
    <div class="container">
        <br><br>
        <div class="row">
            <div class="col-md-3">
                <access-add></access-add>
            </div>
            <div class="col-md-9">
                <access-list :list="{{ $accessList }}"></access-list>
            </div>
        </div>
    </div>

@endsection


@section('page_footer')
    <script>
        _URL.set('access.store', '{{ route('access.store') }}');
        _URL.set('access.update', '{{ route('access.update', ['!access!' => '!access!']) }}');
    </script>
    <script src="{{ mix('/js/admin-accesses.js') }}" defer></script>
@endsection

