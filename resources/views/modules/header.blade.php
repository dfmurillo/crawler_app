<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Crawler</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
          <a class="nav-link" href="{{ route('admin.access') }}">Access</a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.pages') }}" class="nav-link">Pages</a>
      </li>
    </ul>

  </div>
</nav>