<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great![]
|
*/

// Route::get('/', function () {
//     return 
// });

Auth::routes();
Route::get('/', 'HomeController@index')->name('index');

Route::get('/test', 'Admin\AdminController@test');

Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'active']], function(){

    Route::get('/', 'Admin\AdminController@access')->name('admin.access');
    Route::get('/pages', 'Admin\AdminController@pages')->name('admin.pages');
    
    Route::apiResource('access', 'Admin\API\UserAccessController')->except('index');
    Route::apiResource('group', 'Admin\API\UserGroupController')->except('index');
    Route::apiResource('page', 'Admin\API\UserPageController')->except('index');
    Route::apiResource('item', 'Admin\API\UserPageItemController')->except('index');

});