<?php

use Illuminate\Database\Seeder;

use App\GlobalPage;
use App\GlobalItem;

class GlobalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $protocol = 'https';
        $pages = [
            [
                'host' => 'www.exito.com',
                'vendor' => 'exito',
                'items' => [
                    [
                        'dom_item'=>'.col-price .price .money',
                        'comma_separator'=>'.',
                        'thousand_separator'=>',',
                        'item_name'=>'price',
                        'data_clean'=>'numeric',
                    ],
                ],
            ],
            [
                'host' => 'www.larebajavirtual.com',
                'vendor' => 'larebajavirtual',
                'items' => [
                    [
                        'dom_item'=>'.pricened span',
                        'comma_separator'=>',',
                        'thousand_separator'=>'.',
                        'item_name'=>'price',
                        'data_clean'=>'numeric',
                    ],
                    [
                        'item_name' => 'rating',
                        'dom_item' => '#raty-lectura-producto-10286',
                        'dom_attr'=>'data-score',
                        'data_clean'=>'numeric',
                    ],
                ],
            ],
            [
                'host' => 'www.amazon.com.mx',
                'vendor' => 'amazon',
                'items' => [
                    [
                        'dom_item'=>'#priceblock_ourprice',
                        'comma_separator'=>'.',
                        'thousand_separator'=>',',
                        'item_name'=>'price',
                        'data_clean'=>'numeric',
                    ],
                    [
                        'item_name' => 'rating',
                        'dom_item'=>'#averageCustomerReviews .a-icon-star .a-icon-alt',
                        'data_clean'=>'basic',
                    ],
                ],
            ],
            [
                'host' => 'www.heb.com.mx',
                'vendor' => 'heb',
                'items' => [
                    [
                        'dom_item'=>'.price-box .price',
                        'comma_separator'=>'.',
                        'thousand_separator'=>',',
                        'item_name'=>'price',
                        'data_clean'=>'numeric',
                    ],
                ],
            ],
            [
                'host' => 'www.chedraui.com.mx',
                'vendor' => 'chedraui',
                'items' => [
                    [
                        'dom_item'=>'.product-details p',
                        'comma_separator'=>'.',
                        'thousand_separator'=>',',
                        'item_name'=>'price',
                        'data_clean'=>'numeric',
                    ],
                ],
            ],
            [
                'host' => 'super.walmart.com.mx',
                'vendor' => 'walmart',
                'items' => [
                    [
                        'dom_item'=>'basePrice',
                        'item_name'=>'price',
                    ],
                ],
                'type_request'=>'service',
            ],
            [
                'host' => 'www.superama.com.mx',
                'vendor' => 'superama',
                'items' => [
                    [
                        'dom_item'=>'.detail-description-price span',
                        'comma_separator'=>'.',
                        'thousand_separator'=>',',
                        'item_name'=>'price',
                        'data_clean'=>'numeric',
                    ],
                ],
            ],
            [
                'host' => 'stunning-base-164402.appspot.com',
                'vendor' => 'farmatodo',
                'items' => [
                    [
                        'dom_item'=>'fullPrice',
                        'item_name'=>'price',
                    ],
                ],
                'type_request'=>'service',
            ],
            [
                'host' => 'www.tiendasjumbo.co',
                'vendor' => 'jumbo',
                'items' => [
                    [
                        'dom_item'=>'.descricao-preco .skuBestPrice',
                        'comma_separator'=>',',
                        'thousand_separator'=>'.',
                        'item_name'=>'price',
                        'data_clean'=>'numeric',
                    ],
                ],
            ],
        ];

        foreach($pages as $page) {
            $page['protocol'] = $protocol;
            $items = $page['items'];
            unset($page['items']);
            $globalPage = GlobalPage::create($page);

            foreach ($items as $item) {
                $item['global_page_id'] = $globalPage->id;
                $globalItem = GlobalItem::create($item);
            }
        }
    }
}
