<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
		$user->name = 'Daniel Florez';
		$user->email = 'daniel.florez@vml.com';
		$user->password = bcrypt('Studiocom*12');
		$user->status = 'active';
		$user->save();
    }
}
