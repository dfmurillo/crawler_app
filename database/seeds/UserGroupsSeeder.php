<?php

use Illuminate\Database\Seeder;

use App\UserGroup;
use App\UserPage;
use App\UserPageItem;

class UserGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            'suavitel-co'=>[
                'exito'=>[
                    'id' => 1,
                    'status' => 'active',
                    'origin'=>'https://www.exito.com/products/0000378890388983/Suavizante+Fresca+Primaveral+2+X+1000+Ml+pe',
                    'page' => '/products/0000378890388983/Suavizante+Fresca+Primaveral+2+X+1000+Ml+pe',
                    'cookies'=>json_encode(['cookies'=>['selectedCity'=>'BG'], 'path'=>'www.exito.com']),
                    'items' => [
                        'price' => 1,
                    ],
                ],
                'larebaja' => [
                    'id' => 2,
                    'status' => 'active',
                    'origin'=>'https://www.larebajavirtual.com/catalogo/producto/producto/10286/descripcion/OFERTA-SOFLAN-SUAVITEL-PRIMAVERAL-FCO-X-1000-CC-2-P.E.html',
                    'page' => '/catalogo/producto/producto/10286/descripcion/OFERTA-SOFLAN-SUAVITEL-PRIMAVERAL-FCO-X-1000-CC-2-P.E.html',
                    'items' => [
                        'price' => 2,
                        'rating' => 3,
                    ],
                ],
                'farmatodo' => [
                    'id' => 8,
                    'status' => 'active',
                    'origin'=>'https://www.farmatodo.com.co/#/product/1029069/suavizante-suavitel-fresca',
                    'page' => '/_ah/api/productEndpoint/getItem?idItem=1029069&idStoreGroup=26&token=48c280ee654cc7972570fbb8123a578a&tokenIdWebSafe=ahZzfnN0dW5uaW5nLWJhc2UtMTY0NDAycl0LEgRVc2VyIiRmMGMwYmM0Ny1jNjFmLTQ3YTUtOTliNS05OGRjMDdlYzMyZjYMCxIFVG9rZW4iJGY1YjM1MDkzLWUzZDQtNGRlNS1hMGFmLTNlMDA4ZTZmZGZmNAw',
                    'items' => [
                        'price' => 10,
                    ],
                ],
                'jumbo' => [
                    'id' => 9,
                    'status' => 'active',
                    'origin'=>'https://www.tiendasjumbo.co/lavaloza-liquido-doy-pack-axion-limon-espuma-activa-x-1500ml/p',
                    'page' => '/lavaloza-liquido-doy-pack-axion-limon-espuma-activa-x-1500ml/p',
                    'items' => [
                        'price' => 11
                    ],
                ],
            ],
            'suavitel-mx'=>[
                'amazon' => [
                    'id' => 3,
                    'status' => 'active',
                    'origin'=>'https://www.amazon.com.mx/Suavitel-Complete-Fresca-Primavera-Suavizante/dp/B015JCE6FS/ref=sr_1_1?ie=UTF8&qid=1524760606&sr=8-1&keywords=suavitel+complete',
                    'page' => '/Suavitel-Complete-Fresca-Primavera-Suavizante/dp/B015JCE6FS/ref=sr_1_1?ie=UTF8&qid=1524760606&sr=8-1&keywords=suavitel+complete',
                    'items' => [
                        'price' => 4,
                        'rating' => 5,
                    ],
                ],
                'heb' => [
                    'id' => 4,
                    'status' => 'active',
                    'origin'=>'https://www.heb.com.mx/suavitel-suavizante-complete-prim-3lt-3-lt-522098.html',
                    'page' => '/suavitel-suavizante-complete-prim-3lt-3-lt-522098.html',
                    'items' => [
                        'price' => 6
                    ],
                ],
                'chedraui' => [
                    'id' => 5,
                    'status' => 'active',
                    'origin'=>'https://www.chedraui.com.mx/Departamentos/Super/Limpieza-del-Hogar-y-Desechables/Cuidado-de-la-Ropa/Suavizantes/Suavizante-Suavitel-Fresca-Primavera-Bo/p/000000000003444677',
                    'page' => '/Departamentos/Super/Limpieza-del-Hogar-y-Desechables/Cuidado-de-la-Ropa/Suavizantes/Suavizante-Suavitel-Fresca-Primavera-Bo/p/000000000003444677',
                    'items' => [
                        'price' => 7
                    ],
                ],
                'wallmart' => [
                    'id' => 6,
                    'status' => 'active',
                    'origin'=>'https://super.walmart.com.mx/Lavanderia/Acondicionador-de-telas-Suavitel-Adios-al-Planchado-Complete-fresca-primavera-3-l/00750954604443',
                    'page' => '/api/rest/model/atg/commerce/catalog/ProductCatalogActor/getSkuSummaryDetails?storeId=0000009999&upc=00750954604443&skuId=00750954604443',
                    'items' => [
                        'price' => 8
                    ],
                ],
                'superama' => [
                    'id' => 7,
                    'status' => 'active',
                    'origin'=>'https://www.superama.com.mx/catalogo/d-lavanderia-hogar-y-mascotas/f-lavanderia/l-suavizantes-de-ropa/acondicionador-de-telas-suavitel-adios-al-planchado-complete-fresca-primavera-3-l/0750954604443',
                    'page' => '/catalogo/d-lavanderia-hogar-y-mascotas/f-lavanderia/l-suavizantes-de-ropa/acondicionador-de-telas-suavitel-adios-al-planchado-complete-fresca-primavera-3-l/0750954604443',
                    'items' => [
                        'price' => 9
                    ],
                ],
            ],
        ];

        foreach($groups as $groupName => $pages) {
            $groupObj = new UserGroup;
            $groupObj->user_id = 1;
            $groupObj->name = $groupName;
            $groupObj->save();

            foreach($pages as $pageName => $page){
                $userPage = new UserPage;
                $userPage->global_page_id = $page['id'];
                $userPage->user_group_id = $groupObj->id;
                $userPage->slug = $groupName.'-'.$pageName;
                $userPage->status = 'active';
                if(isset($page['cookies'])){
                    $userPage->cookies  = $page['cookies'];
                }
                $userPage->origin = $page['origin'];
                $userPage->page = $page['page'];
                $userPage->save();

                foreach($page['items'] as $itemName => $item) {
                    $userItem = new UserPageItem;
                    $userItem->user_page_id = $userPage->id;
                    $userItem->global_item_id = $item;
                    $userItem->name = $itemName;

                    $userItem->save();
                }
            }
        }
    }
}
