<?php

use Illuminate\Database\Seeder;

use App\UserAccess;

class AccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = new UserAccess;
        $access->user_id = 1;
        $access->domain = 'https://tuhogar.com';
        $access->token = '3cxwsWuqVC5ZpkcnRrJe';
        $access->save();
    }
}
