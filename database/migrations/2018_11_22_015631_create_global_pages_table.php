<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('protocol');
            $table->string('host');
            $table->string('vendor');
            $table->enum('type_request', ['service','crawl'])->default('crawl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_pages');
    }
}
