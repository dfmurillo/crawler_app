<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('global_page_id')->unsigned();
            $table->string('item_name')->nullable();
            $table->string('dom_item')->nullable();
            $table->integer('dom_item_eq')->nullable();
            $table->string('dom_attr')->nullable();
            $table->string('comma_separator', 1)->nullable();
            $table->string('thousand_separator', 1)->nullable();
            $table->enum('data_clean', ['numeric', 'alpha', 'basic'])->nullable();
            $table->timestamps();

            $table->foreign('global_page_id')->references('id')->on('global_pages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_items');
    }
}
