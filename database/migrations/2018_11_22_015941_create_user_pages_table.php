<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('global_page_id')->unsigned();
            $table->integer('user_group_id')->unsigned();
            $table->text('cookies')->nullable();
            $table->text('origin');
            $table->text('page');
            $table->string('slug');
            $table->enum('status', ['active', 'inactive', 'process', 'draft'])->default('process');
            $table->timestamps();

            $table->foreign('global_page_id')->references('id')->on('global_pages');
            $table->foreign('user_group_id')->references('id')->on('user_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_pages');
    }
}
