<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPageItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_page_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_page_id')->unsigned();
            $table->integer('global_item_id')->unsigned();
            $table->string('name');
            $table->timestamps();
            
            $table->foreign('user_page_id')->references('id')->on('user_pages');
            $table->foreign('global_item_id')->references('id')->on('global_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_page_items');
    }
}
