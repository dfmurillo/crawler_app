<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserItemHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_item_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_page_item_id')->unsigned();
            $table->string('value')->nullable();
            $table->timestamps();

            $table->foreign('user_page_item_id')->references('id')->on('user_page_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_item_history');
    }
}
