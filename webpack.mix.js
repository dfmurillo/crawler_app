const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
      postCss: [
         require('autoprefixer')({
            browsers: ['last 4 versions'],
            cascade: false
         })
      ]
   })
   .js('resources/js/pages/admin-accesses.js', 'public/js')
   .js('resources/js/pages/admin-pages.js', 'public/js')
   .sass('resources/sass/compile/main.scss', 'public/css')
   .extract(['vue','axios','jquery'])
   .sourceMaps()
   .browserSync('local.pricecrawlerapp.com');
